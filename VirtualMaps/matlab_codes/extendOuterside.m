function [mapOut, xOut, yOut] = extendOuterside( mapIn, xIn, yIn, rfit, rOut, zernikeN )
% function datOut = extendOuterside( datIn, xl, yl, rfit, rout, zernikeN )
% fit dat in radius of rfit using zernikeCoef the index of zernikeN
% use the coefficient to expand data in the ring between r = rfit ~ rout

% Octoer 31, 2022
%  Argument zernikeN explained, indMax is properly replaced by zernikeN
% 
% April 27, 2021
%  The junction between the raw data and fit are connected smoothly.
%  If rfit = 0, the raw data boudary set by 0 or nan is used.
%
% June 27, 2020
%  When rout < 0, xOut and yOut should be xIn and yIn
%
% June 19, 2020
%   When rout is outside of xl, xlOut is larger than xl
%
% March 6th, 2020
%   outside region was filled improperly
%
% December 17, 2014  Hiro Yamamoto
%   inital code

if ~exist( 'zernikeN', 'var' ) || zernikeN == 0
    % using smaller index seems to be better than using a large index, like 17.
    zernikeN = 5;
end

if rfit == 0
    [xL, xH, yL, yH] = UtilTK.findBoundary( mapIn, xIn, yIn, true );
    rfit = min( (xH-xL), (yH-yL) )/2;
end

% try positive and negetive zernikeN
% calculate the zernite terms
% -zernikeN means : for positive zernikeN, use all (0,0),(1,-1),(1,1),...,(zernikeN,-zernikeN) ...,(zernikeN,zernikeN)
%               : for negative zernikeN, use lower order wyko indexes from 0 to (-zernikeN)
% datDel = datIn - fit, 
% fit = datIn - datDel
% datOut( r < rfit ) = datIn
% datOut( r > rfit ) = fit
[~, coef, nmList] = zernikeCoef( mapIn, xIn, yIn, rfit*2, zernikeN ); % datDel = sum of zernike terms of lower order modes

if min(size(xIn)) > 1
    xIn = xIn(1,:);
end
if min(size(yIn)) > 1
    yIn = yIn(:,1);
end

if rOut < 0
    % use xIn and yOut as xOut and yOut
    rOut = -rOut;
    xOut = xIn;
    yOut = yIn;
    N0X = 0;
    N0Y = 0;
else
    dxl = xIn(2) - xIn(1);
    dyl = yIn(2) - yIn(1);
    dN = rOut + xIn(1);
    if dN > 0
        N0X = ceil(dN/dxl);
    else
        N0X = 0;
    end
    N = ceil(max(rOut-xIn(end),0)/dxl) + N0X + length(xIn);
    xOut = xIn(1) + dxl*((1:N) - N0X - 1);  % xl(N0+1) =  xl(1)
    dN = rOut + yIn(1);
    if dN > 0
        N0Y = ceil(dN/dyl);
    else
        N0Y = 0;
    end
    N = ceil(max(rOut-yIn(end),0)/dyl) + N0Y + length(yIn);
    yOut = yIn(1) + dyl*((1:N) - N0Y - 1);  % yl(N0+1) =  yl(1)
end

[xg, yg] = meshgrid(xOut, yOut);
mapOut = nan( size(xg) );
mapOut(N0Y+1:N0Y+length(yIn), N0X+1:N0X+length(xIn)) = mapIn;

dr = xOut(2) - xOut(1);
rg = sqrt( xg.^2 + yg.^2 );
nmList(:,3) = coef(:);
datFit = zernikeSum( xg, yg, nmList, rfit, rOut+dr );
rng = rg >= rfit & rg <= abs(rOut)+dr;
mapOut( rng ) = datFit( rng );

xeps = 0.1;
wgt = sqrt(1-1/xeps*(1-rg/rfit));
rng = (rg > rfit*(1-xeps)) & (rg < rfit);
mapOut( rng ) = datFit( rng ) .* wgt(rng) + mapOut(rng) .* (1-wgt(rng));


