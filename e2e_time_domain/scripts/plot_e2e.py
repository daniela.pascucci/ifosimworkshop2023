#!/usr/bin/env python

# Quick and dirty data-viewer for e2e output
# Bas Swinkels, 2012

import matplotlib.pyplot as plt
import numpy as np
import sys
import os.path as path
import argparse

parser = argparse.ArgumentParser(
  description = 'makes time-domain plots of e2e output')
parser.add_argument('filename',
  help='name of data files, both filename.dat and filename.dhr must exist')
parser.add_argument('-l', '--log', action = 'store_true',
  help = 'changes y-axis of plots to logarithmic scale')
parser.add_argument('channels', nargs = '*', metavar = 'channel',
  help = 'channel names to plot (all channels are plotted by default)')
args = parser.parse_args()

name = path.splitext(args.filename)[0]

try:
    f = open(name + '.dhr')
    header = [line.strip() for line in f]
    f.close()
except:
    sys.exit('Error: could not read %s.dhr' % name)

def search(chan):
    'return index in header. also allows partial name if it is unique'
    indices = [i for (i, h) in enumerate(header) if chan in h]
    if len(indices) == 0:
        sys.exit('Error: unknown channel %s' % chan)
    if len(indices) > 1:
        sys.exit('Error: more than one channel contains %s' % chan)
    return indices[0]

try:
    data = np.genfromtxt(name + '.dat')
except:
    sys.exit('Error: could not read %s.dat' % name)

if args.channels: #plot specific channels
    index = [search(chan) for chan in args.channels]
else: #plot all channels in order of header
    index = range(1, len(header))

nplot = len(index)
time = data[:,0]

fig, axes = plt.subplots(nplot, 1, sharex=True)
if nplot == 1: axes = [axes] #hack so that you can always iterate over axes

fig.subplots_adjust(left = 0.1, right = 0.95, top = 0.95, bottom = 0.1, hspace = 0.1)

xl = [0, time[-1]]

for iplot, ichan in enumerate(index):
    ax = axes[iplot]
    
    if args.log:       
        ax.semilogy(time, data[:,ichan], '-')
    else:
        ax.plot(time, data[:,ichan], '-', xl, [0, 0], 'k')
    if iplot != nplot - 1: #bug: plt.subplots should have done this already
        plt.setp(ax.get_xticklabels(), visible=False)
    #ax.yaxis.set_ticklabels('')
    ax.grid()
    ax.set_ylabel(header[ichan])
    
plt.xlabel('Time (s)')
plt.xlim(xl)
plt.show()
