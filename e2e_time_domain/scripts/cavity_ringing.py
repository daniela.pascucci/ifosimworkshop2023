# toy time domain simulation of cavity ringing effect

import numpy as np
import matplotlib.pyplot as plt

# parameters 
c = 3e8  # m/s
wl = 1.0e-6  # m
k = 2*np.pi/ wl
Lcav = 3000  # m
R1 = R2 = 0.995
T1 = T2 = 1 - R1
r1, t1, r2, t2 = np.sqrt([R1, T1, R2, T2])
v = 1e-6  # m/s

dt = Lcav / c  # set timestep equal to light travel time
ntotal = 120000
t = (np.arange(ntotal) - ntotal//2) * dt
Etra = np.full_like(t, np.nan, dtype=complex)
Eref = np.full_like(t, np.nan, dtype=complex)
x = t[0] * v

# init fields
Elaser = 1
E1 = 0
E3 = 0

for i in range(ntotal):
    x = t[i] * v    

    # propagate fields from previous step
    P = np.exp(-1j * k * x)
    E2 = P * E1
    E4 = P * E3

    # instantaneous propagation
    E1 = t1 * Elaser + r1 * E4
    Eref[i] = t1 * E4 - r1 * Elaser
    Etra[i] = t2 * E2
    E3 = r2 * E2

Ptra = abs(Etra)**2
Pref = abs(Eref)**2
Ptot = Ptra + Pref

plt.plot(t, Ptra)
plt.xlabel('Time (s)')
plt.ylabel('Transmitted power (W)')
plt.xlim([-0.01, 0.02])
plt.grid(True)
plt.title('Cavity ringing effect')
plt.show()


