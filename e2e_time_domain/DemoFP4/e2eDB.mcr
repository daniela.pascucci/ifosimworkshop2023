#print "DempFP"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GLOBAL
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

HZ = -2 * PI
NEVER = 1e6

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Optics
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Optics.in_pow = 1

% modulation
fMod = 24.482e6 [Hz]		"modulation frequency
gammaMod = 0.45 [1]		"modulation depth
orderMod = 2 [1]		"modulation expansion order
kMod = 2 * PI * fMod / LIGHT_SPEED

% Cavity
length = 80 [m]			"cavity length

T_ITM = 0.03
L_ITM = 100e-6

T_ETM = 1e-3
L_ETM = 100e-6


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Control
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

j0 = jbessel(0, gammaMod)
rr = sqrt(1 - T_ITM - L_ITM) * sqrt(1 - T_ETM - L_ETM)
maxPtr = (j0 * j0) * T_ETM * T_ITM / pow(1 - rr, 2)

lockOn = 0.2
lockOff = 0.1
lockWait = 2e-4
runOn = 0.9

%outLimit = 0.01 [N]		"maximum output force
%outLimit = 0.1 [N]		"maximum output force
outLimit = 0.5 [N]		"maximum output force


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Radiation Pressure
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
y_off = 0.014
