#print "ET_arm"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GLOBAL
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

HZ = -2 * PI
NEVER = 1e6

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Optics
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Optics.P_in = 3

% modulation
fMod = 11363101 [Hz]	"modulation frequency
gammaMod = 0.45 [1]		"modulation depth
orderMod = 1 [1]		"modulation expansion order
kMod = 2 * PI * fMod / LIGHT_SPEED

% Cavity
length = 10e3 [m]		"cavity length

T_ITM = 7000e-6x`
L_ITM = 37.5e-6
Rc_ITM = 5580

T_ETM = 6e-6
L_ETM = 37.5e-6
Rc_ETM = 5580


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Control
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

j0 = jbessel(0, gammaMod)
rr = sqrt(1 - T_ITM - L_ITM) * sqrt(1 - T_ETM - L_ETM)
maxPtr = (j0 * j0) * T_ETM * T_ITM / pow(1 - rr, 2)

lockOn = 0.2
lockOff = 0.1
lockWait = 2e-4
runOn = 0.9

outLimit = 0.01 [N]		"maximum output force

MechITM.rad_on = 0